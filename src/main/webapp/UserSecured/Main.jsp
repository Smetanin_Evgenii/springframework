<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 28.11.2021
  Time: 19:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Main</title>
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css2?family=Lato:ital,wght@1,300..');

        html {
            font-family: 'Montserrat', sans-serif;
            font-size: 16px;
            line-height: 24px;
            font-weight: 400;
            color: #222;
        }

        p {
            font-size: 16px;
            line-height: 24px;
            font-weight: 400;
            color: #222;
        }

        div {
            font-size: 16px;
            line-height: 24px;
            font-weight: 400;
            color: #222;
        }

        h1 {
            font-size: 48px;
            line-height: 54px;
            font-weight: 700;
        }

        h2 {
            font-size: 36px;
            line-height: 42px;
            font-weight: 700;
        }

        h3 {
            font-size: 28px;
            line-height: 36px;
            font-weight: 700;
        }
    </style>
</head>
<body>
<h2>Please enter your coordinates to search for transport, or input transport's id to rent a specific transport.</h2>

<form action="login" method="post">
    <div class="container">

        <label for="latitude"><b>Latitude</b></label>
        <input type="text" placeholder="Latitude" name="latitude">

        <label for="longitude"><b>Longitude</b></label>
        <input type="text" placeholder="Longitude" name="longitude">

        <label for="transportId"><b>TransportId</b></label>
        <input type="text" placeholder="TransportId" name="transportId">

    </div>
</form>

<form action="findBicycle" method="post">
    <div class="container">

        <input type="submit" name="findBicycle" value="Find Bicycle">

    </div>
</form>

<form action="findScooter" method="post">
    <div class="container">

        <input type="submit" name="findScooter" value="Find Scooter">

    </div>
</form>

<form action="rentTransport" method="post">
    <div class="container">

        <input type="submit" name="rentTransport" value="rent Transport">

    </div>
</form>

<p><c:out value="${error}"/></p>
</body>
</html>
