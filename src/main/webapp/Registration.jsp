<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 28.11.2021
  Time: 14:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css2?family=Lato:ital,wght@1,300..');

        html {
            font-family: 'Montserrat', sans-serif;
            font-size: 16px;
            line-height: 24px;
            font-weight: 400;
            color: #222;
        }

        p {
            font-size: 16px;
            line-height: 24px;
            font-weight: 400;
            color: #222;
        }

        div {
            font-size: 16px;
            line-height: 24px;
            font-weight: 400;
            color: #222;
        }

        h1 {
            font-size: 48px;
            line-height: 54px;
            font-weight: 700;
        }

        h2 {
            font-size: 36px;
            line-height: 42px;
            font-weight: 700;
        }

        h3 {
            font-size: 28px;
            line-height: 36px;
            font-weight: 700;
        }
    </style>
</head>
<body>
<h2>Please enter your email and password to register.</h2>
<form action="registration" method="post">
    <div class="container">

        <label for="login"><b>Login</b></label>
        <input type="text" placeholder="Login" name="login" required>

        <label for="password"><b>Password</b></label>
        <input type="text" placeholder="Password" name="password" required>

        <label for="confirmPassword"><b>ConfirmPassword</b></label>
        <input type="text" placeholder="ConfirmPassword" name="confirmPassword" required>

        <button type="submit">Register</button>
    </div>
</form>
<p><c:out value="${error}"/></p>
</body>
</html>
