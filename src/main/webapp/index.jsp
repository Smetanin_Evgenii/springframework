<html>
<head>
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css2?family=Lato:ital,wght@1,300..');

        html {
            font-family: 'Montserrat', sans-serif;
            font-size: 16px;
            line-height: 24px;
            font-weight: 400;
            color: #222;
        }

        p {
            font-size: 16px;
            line-height: 24px;
            font-weight: 400;
            color: #222;
        }

        div {
            font-size: 16px;
            line-height: 24px;
            font-weight: 400;
            color: #222;
        }

        h1 {
            font-size: 48px;
            line-height: 54px;
            font-weight: 700;
        }

        h2 {
            font-size: 36px;
            line-height: 42px;
            font-weight: 700;
        }

        h3 {
            font-size: 28px;
            line-height: 36px;
            font-weight: 700;
        }
    </style>
</head>
<body>
<h2>Welcome to SystemCityTransport. Please Login or Register.</h2>

<form action="login" method="post">
    <div class="container">
        <input type="submit" name="login" value="Login">
    </div>
</form>

<form action="registration" method="post">
    <div class="container">
        <input type="submit" name="register" value="Register">
    </div>
</form>

<p><c:out value="${error}"/></p>
</body>
</html>
