package configuration;

import access.DatabaseConnection;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(
        value = "classpath:/DBConnection.properties"
)
@ComponentScan(basePackages = {"access", "servlets"})
public class JavaConfiguration {

    @Bean
    public DatabaseConnection DatabaseConnection() {
        System.out.println("Getting database connection...");
        return DatabaseConnection.getInstance();
    }
}
