package access;

import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

@Repository
public class DatabaseConnection {
    private static DatabaseConnection instance;
    private Connection con = null;
    private Statement stmt = null;
    private ResultSet rs = null;
    static String dbHost;
    static String dbLogin;
    static String dbPassword;

    private DatabaseConnection() {
    }

    public static DatabaseConnection getInstance() {

        if (instance == null) {
            synchronized (DatabaseConnection.class) {
                if (instance == null) {

                    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
                    InputStream input = classLoader.getResourceAsStream("DBConnection.properties");
                    Properties prop = System.getProperties();

                    try {
                        prop.load(input);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    dbHost = prop.getProperty("db.host");
                    dbLogin = prop.getProperty("db.login");
                    dbPassword = prop.getProperty("db.password");
                    instance = new DatabaseConnection();
                }
            }
        }
        return instance;
    }

    public void connectToDatabase() {
        try {
            DriverManager.registerDriver(new org.h2.Driver());

            con = DriverManager.getConnection(
                    dbHost,
                    dbLogin,
                    dbPassword
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void executeStatement(String statement) {
        if (con == null) {
            connectToDatabase();
        }
        try {
            stmt = con.createStatement();
            stmt.execute(statement);
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String executeSelect(String statement) {
        StringBuilder result = new StringBuilder();
        if (con == null) {
            connectToDatabase();
        }
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(statement);
            int index = 1;
            while (rs.next()) {
                result.append(rs.getString(index));
                index = 1;
            }
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    public void disconnectFromDatabase() {
        try {
            if (rs != null) rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (stmt != null) stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (con != null) con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
