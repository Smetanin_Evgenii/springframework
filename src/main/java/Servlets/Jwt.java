package servlets;

import model.dto.UserDto;
import services.JwtCreationService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Jwt", urlPatterns = "/jwt")
public class Jwt extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");

        HttpSession session = req.getSession(true);
        UserDto userDto = (UserDto) session.getAttribute("user");
        String token = JwtCreationService.createJwt(userDto);
        PrintWriter out = resp.getWriter();
        out.print(token);
        out.flush();
    }

}
