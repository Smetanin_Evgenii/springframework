package servlets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.UserAccessService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;

@WebFilter(urlPatterns = "/userSecured/*")
public class UserFilter implements Filter {

    public final static Logger LOG = LoggerFactory.getLogger(UserAccessService.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpSession session = req.getSession(false);
        if (session != null) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            LOG.warn("Unauthorized access attempt {}", LocalDateTime.now());
            throw new ServletException("You are not logged in!");
        }
    }

    @Override
    public void destroy() {
    }
}
