package servlets;

import model.dto.UserDto;
import model.entity.User;
import services.UserAccessService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        req.getRequestDispatcher("/Login.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        ServletContext servletContext = getServletContext();
        try {
            User user = UserAccessService.login(login, password);
            UserDto userDto = new UserDto(user);
            HttpSession session = req.getSession(true);
            session.setAttribute("user", userDto);
            resp.setCharacterEncoding("UTF-8");
            resp.sendRedirect(
                    req.getContextPath() + "/userSecured/ui/main.jsp");
        } catch (Exception e) {
            e.getMessage();
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            req.setAttribute("error", "Invalid login or password");
            servletContext.getRequestDispatcher("/Login.jsp").forward(req, resp);
        }
    }
}
