package services;

import access.DatabaseConnection;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;
import model.entity.User;
import model.entity.UserStatusEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class UserAccessService {

    public final static Logger LOG = LoggerFactory.getLogger(UserAccessService.class);

    private UserAccessService() {
    }

    public static User login(String login, String password) {
        String correctPassword;
        User user = null;

        DatabaseConnection.getInstance().connectToDatabase();
        correctPassword = DatabaseConnection.getInstance().executeSelect(
                "SELECT user_password FROM user WHERE user_email = '" + login + "'");

        try {
            if (correctPassword.equals("")) throw new WrongLoginException("User with such login not found!");
            if (!password.equals(correctPassword)) throw new WrongPasswordException("Incorrect password!");
            user = createUser(login);
            LOG.debug("User {} successfully logged in at {}", user.getEmail(), LocalDateTime.now());
        } catch (WrongLoginException | WrongPasswordException e) {
            e.getMessage();
            if ((!login.isBlank() || !login.isEmpty()) && !login.equals("Login")) {
                LOG.info("User {} login failed at {}", login, LocalDateTime.now());
                LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
                StatusPrinter.print(lc);
            }
        } finally {
            DatabaseConnection.getInstance().disconnectFromDatabase();
        }
        return user;
    }

    private static User createUser(String login) {
        UUID id;
        String email;
        String password;
        long balance;
        int numberOfRides;
        UserStatusEnum status;

        id = UUID.fromString(DatabaseConnection.getInstance().executeSelect(
                "SELECT user_id, FROM user WHERE user_email = '" + login + "'"));
        email = login;
        password = DatabaseConnection.getInstance().executeSelect(
                "SELECT user_password, FROM user WHERE user_email = '" + login + "'");
        balance = Long.parseLong(DatabaseConnection.getInstance().executeSelect(
                "SELECT user_balance, FROM user WHERE user_email = '" + login + "'"));
        numberOfRides = Integer.parseInt(DatabaseConnection.getInstance().executeSelect(
                "SELECT user_number_of_rides, FROM user WHERE user_email = '" + login + "'"));
        status = UserStatusEnum.valueOf(DatabaseConnection.getInstance().executeSelect(
                "SELECT user_role, FROM user WHERE user_email = '" + login + "'").toUpperCase());

        User user = new User(id, email, password, balance, numberOfRides, status);
        return user;
    }
}
