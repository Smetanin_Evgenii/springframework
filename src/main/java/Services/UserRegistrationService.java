package services;

import access.DatabaseConnection;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserRegistrationService {
    private static Pattern passwordPattern = Pattern.compile("[A-Za-z0-9_]{0,19}");
    private static Pattern loginPattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,19}$", Pattern.CASE_INSENSITIVE);

    private UserRegistrationService() {
    }

    public static boolean registerNewUser(String email, String password, String confirmPassword) {
        boolean isSuccessful = false;
        BigInteger newUuid = getUniqueUserId();

        if (checkLoginPattern(email) && isLoginUnique(email) && checkPassword(password, confirmPassword)) {
            DatabaseConnection.getInstance().connectToDatabase();
            DatabaseConnection.getInstance().executeStatement(
                    "INSERT INTO user VALUES (" +
                            newUuid + ", '" + email + "', '" + password + "', 0, 0, 'RENTER');"
            );
            isSuccessful = checkRegistrationSuccess(newUuid.toString(), email, password);
        }
        DatabaseConnection.getInstance().disconnectFromDatabase();
        return isSuccessful;
    }

    private static boolean checkRegistrationSuccess(String id, String email, String password) {
        boolean isRegistrationSuccessful = false;

        DatabaseConnection.getInstance().connectToDatabase();
        if (email.equals(DatabaseConnection.getInstance().executeSelect(
                "SELECT user_email, FROM user WHERE user_id = " + id + ";"))
                && password.equals(DatabaseConnection.getInstance().executeSelect(
                "SELECT user_password, FROM user WHERE user_id = " + id + ";"))) {
            isRegistrationSuccessful = true;
        }
        DatabaseConnection.getInstance().disconnectFromDatabase();
        return isRegistrationSuccessful;
    }

    private static BigInteger getUniqueUserId() {
        String maxId;

        DatabaseConnection.getInstance().connectToDatabase();
        maxId = DatabaseConnection.getInstance().executeSelect(
                "SELECT user_id FROM user ORDER BY user_id DESC LIMIT 1")
                .replace("-", "");
        BigInteger maxUuid = new BigInteger(maxId, 16);
        BigInteger newUuid = maxUuid.add(new BigInteger("1", 16));
        DatabaseConnection.getInstance().disconnectFromDatabase();
        return newUuid;
    }

    private static boolean checkLoginPattern(String email) {
        Matcher checkLogin = loginPattern.matcher(email);
        boolean isEmailCorrect = false;

        try {
            if (checkLogin.matches()) {
                isEmailCorrect = true;
            } else {
                throw new WrongLoginException("Incorrect email format");
            }
        } catch (WrongLoginException e) {
            System.out.println(e.getMessage());
        }
        return isEmailCorrect;
    }

    private static boolean isLoginUnique(String email) {
        boolean isLoginUnique = false;

        DatabaseConnection.getInstance().connectToDatabase();
        try {
            if (DatabaseConnection.getInstance().executeSelect(
                    "SELECT user_email, FROM user WHERE user_email = '" + email + "';"
            ).equals("")) {
                isLoginUnique = true;
            } else {
                throw new WrongPasswordException("User with such login already exists");
            }
        } catch (WrongPasswordException e) {
            e.printStackTrace();
        }
        DatabaseConnection.getInstance().disconnectFromDatabase();
        return isLoginUnique;
    }

    private static boolean checkPassword(String password, String confirmPassword) {
        Matcher checkPassword = passwordPattern.matcher(password);
        boolean isPasswordCorrect = false;

        try {
            if (password.equals(confirmPassword)) {
                if (checkPassword.matches()) {
                    isPasswordCorrect = true;
                } else {
                    throw new WrongPasswordException("Incorrect password format");
                }
            } else {
                throw new WrongPasswordException("Passwords do not match");
            }
        } catch (WrongPasswordException e) {
            System.out.println(e.getMessage());
        }
        return isPasswordCorrect;
    }
}
