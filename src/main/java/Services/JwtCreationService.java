package services;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import model.dto.UserDto;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtCreationService {
    static final String SECRET = "secret";

    public static String createJwt(UserDto user) {

        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        Map<String, Object> claims = new HashMap<>();
        claims.put("login", user.getEmail());
        claims.put("role", user.getStatus());

        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + 60*60*500000);

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(user.getId().toString())
                .setIssuedAt(now)
                .setExpiration(expiryDate)
                .signWith(signatureAlgorithm, SECRET).compact();
    }
}
