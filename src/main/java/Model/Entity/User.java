package model.entity;

import java.util.UUID;

public class User {
    private UUID id;
    private String email;
    private String password;
    private long balance;
    private int numberOfRides;
    UserStatusEnum status;

    public User(UUID id, String email, String password, long balance, int numberOfRides, UserStatusEnum status) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.balance = balance;
        this.numberOfRides = numberOfRides;
        this.status = status;
    }

    public UUID getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public long getBalance() {
        return balance;
    }

    public int getNumberOfRides() {
        return numberOfRides;
    }

    public UserStatusEnum getStatus() {
        return status;
    }
}