package model.dto;

import model.entity.User;
import model.entity.UserStatusEnum;

import java.util.UUID;

public class UserDto {
    private UUID id;
    private String email;
    private long balance;
    private int numberOfRides;
    UserStatusEnum status;

    public UserDto(User user) {
        this.id = user.getId();
        this.email = user.getEmail();
        this.balance = user.getBalance();
        this.numberOfRides = user.getNumberOfRides();
        this.status = user.getStatus();
    }

    public UUID getId() {
        return id;
    }


    public String getEmail() {
        return email;
    }


    public long getBalance() {
        return balance;
    }

    public int getNumberOfRides() {
        return numberOfRides;
    }

    public UserStatusEnum getStatus() {
        return status;
    }
}
